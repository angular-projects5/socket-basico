import { Component, OnInit } from '@angular/core';
import { ChatServiceService } from './services/chat-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(
    private chatService: ChatServiceService
  ) { }

  ngOnInit() {
    this.chatService.getMessagesPrivate()
      .subscribe(msg => {
        console.log(msg);
      });
  }



}
