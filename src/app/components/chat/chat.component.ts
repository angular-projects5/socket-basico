import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatServiceService } from 'src/app/services/chat-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  texto = '';
  mensajesSubscription: Subscription;
  elemento: HTMLElement;

  mensajes: any[] = [];

  constructor(
    private chatService: ChatServiceService
  ) { }

  ngOnInit(): void {
    this.elemento = (document.getElementsByClassName('chat-mensajes')[0] as HTMLElement);
    this.mensajesSubscription = this.chatService.getMessages().subscribe(msg => {

      this.mensajes.push(msg);
      setTimeout(() => {
        this.elemento.scrollTop = this.elemento.scrollHeight;
      }, 50);
    });
  }
  ngOnDestroy(): void {
    this.mensajesSubscription.unsubscribe();
  }

  enviar() {
    if (!this.texto.trim().length) { return; }

    this.chatService.sendMessage(this.texto);
    this.texto = '';
  }

}
